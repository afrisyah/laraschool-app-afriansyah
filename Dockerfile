FROM ubuntu:20.04


RUN apt update -y && \
    apt install -y tzdata && \
    ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime && \
    apt install -y apache2 \
    php \
    libapache2-mod-php \
    php-mbstring \
    php-mysql \
    mysql-client \
    php-xml \
    php-gd \
    php-dev \
    php-cli \
    git \
    unzip \
    curl \
    libcurl4 \
    php-curl \
    php-pear \
    libmcrypt-dev \
    libreadline-dev iputils-ping vim nano mysql-client
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php && \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer

RUN mkdir /var/www/laraschool
ADD . /var/www/laraschool
ADD laraschool.conf /etc/apache2/sites-available/
RUN a2enmod rewrite ssl && \
    a2dissite 000-default.conf && \
    a2ensite laraschool.conf
WORKDIR /var/www/laraschool
EXPOSE 8000
RUN ./install.sh
RUN chown www-data:www-data /var/www/laraschool -R
RUN chmod -R 755 /var/www/laraschool
CMD php artisan serve --host=0.0.0.0 --port=8000
EXPOSE 8000

